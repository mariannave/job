![](https://www.estudar.org.br/wp-content/themes/fundacao-estudar-2/images/logo-header.png)

### O que é o projeto

Esta é uma aplicação que demonstra como integrar o backend Rails com um serviço externo utilizando Interactors para encapsular a lógica de programação e programação assíncrona com ajuda do Sidekiq.

### Quais as dependências
```
Rails Version: 5.2.1
Ruby Version: 2.5.3
Mysql version: 5.7 or higher
Redis version: 4.0 or higher
```

### Como instalar

1. Clone o projeto
```
git clone https://gitlab.com/mariannave/job.git
```

2. Acesse a pasta do projeto

3. Rode o comando ```bundle install``` para instalar as bibliotecas

4. Altere o arquivo ```.env``` e adicione as credências de seu banco mysql e redis

5. No terminal rode o comando ```rake db:create``` para criar o banco e ```rake db:migrate``` para rodar as migrações

### Como rodar


Rode o comando ```bundle exec sidekiq -q default``` no terminal

Em outra janela rode o comando ```rails server```

Copie e cole no navegador o endereço abaixo para visualizar a aplicação

```http://localhost:3000/```


### Como rodar os testes

Para rodar os testes, abra o terminal e digite o seguinte comando:

```rspec```
