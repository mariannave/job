class UsersController < ApplicationController

  def index
    @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)

    if @user.valid?
      flash[:alert] = "We are processing your registration."
      ProcessUserJob.perform_later(user_params)
      redirect_to users_path
    else
      render :new
    end
  end

  private
    def user_params
      params.require(:user).permit(:name, :last_name, :job, :remote_id)
    end
end
