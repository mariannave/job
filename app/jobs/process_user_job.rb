class ProcessUserJob < ApplicationJob
  queue_as :default

  def perform(args)
    ProcessUser.call(args)
  end
end
