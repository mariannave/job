require 'net/http'
require 'json'

class ReqresApiService  
  def self.get_users_by_page(page = 1)
    uri = URI.parse("https://reqres.in/api/users?page=#{page}&per_page=12")
    res = Net::HTTP.get(uri)
    return JSON.parse(res)
  end

  def self.user_exists?(users, first_name, last_name)
    users.each do |user|
      return true if user['first_name'].upcase == first_name.upcase and 
        user['last_name'].upcase == last_name.upcase
    end
    return false
  end
  
  def self.search_user(first_name, last_name)
    response = get_users_by_page()  
    return true if user_exists?(response['data'], first_name, last_name)
    
    for page in 2..response['total_pages']
      response = get_users_by_page(page)
      return true if user_exists?(response['data'], first_name, last_name)
    end

    return false
  end

  def self.create_user(name, last_name, job)
    if search_user(name, last_name)
      return false
    else    
      uri = URI('https://reqres.in/api/users')
      res = Net::HTTP.post_form(uri, 'name' => name, 'last_name' => last_name, 'job' => job)
      return JSON.parse(res.body)
    end
  end

end