require 'interactor'

class CreateUserInDatabase
  include Interactor

  def call
    begin
      user = User.create!(:name => context.name,
        :last_name => context.last_name,
        :job => context.job,
        :remote_id => context.remote_id)
      context.user = user
    rescue => exception
      context.fail!(message: exception.message)
    end
  end

end