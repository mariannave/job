require 'interactor'

class ProcessUser
  include Interactor::Organizer

  organize FindUserInAPI, CreateUserInApi, CreateUserInDatabase
end
