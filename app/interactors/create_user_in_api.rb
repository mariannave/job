require 'interactor'

class CreateUserInApi
  include Interactor

  def call
    if response = ReqresApiService.create_user(context.name, context.last_name, context.job)
      context.remote_id = response['id']
    else
      context.fail!(message: "created_user_in_api.failure")
    end
  end
end