require 'interactor'

class FindUserInAPI
  include Interactor

  def call
    if ReqresApiService.search_user(context.name, context.last_name)
      context.fail!(message: 'find_user_in_api.failure')
    end
  end
end