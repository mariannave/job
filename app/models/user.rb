class User < ApplicationRecord
    validates :name, length: { minimum: 2 }
    validates :last_name, length: { minimum: 2 }
    validates :job, length: { minimum: 2 }
end
