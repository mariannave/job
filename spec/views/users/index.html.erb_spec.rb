require 'rails_helper'

RSpec.describe "users/index", type: :view do
  before(:each) do
    assign(:users, [
      User.create!(
        :name => "Name",
        :last_name => "Last Name",
        :job => "Job",
        :remote_id => 2
      ),
      User.create!(
        :name => "Name 2",
        :last_name => "Last Name 2",
        :job => "Job",
        :remote_id => 2
      )
    ])
  end

  it "renders a list of users" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 1
    assert_select "tr>td", :text => "Last Name".to_s, :count => 1
    assert_select "tr>td", :text => "Job".to_s, :count => 2
    assert_select "tr>td", :text => 2.to_s, :count => 2
  end
end
