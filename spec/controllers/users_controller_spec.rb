require 'rails_helper'
require 'vcr'


RSpec.describe UsersController, type: :controller do

  before do
    WebMock.allow_net_connect!
    VCR.turn_off!
  end

  let(:valid_attributes) {
    FactoryBot.attributes_for(:user)
  }

  let(:invalid_attributes) {
    { :name => nil, :last_name => nil, :job => nil}
  }

  let(:valid_session) { {} }

  describe "GET #index" do
    it "returns a success response" do
      User.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe "GET #new" do
    it "returns a success response" do
      get :new, params: {}, session: valid_session
      expect(response).to be_successful
    end
  end

  describe "POST #create" do
    context "with invalid params" do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: {user: invalid_attributes}, session: valid_session
        expect(response).to render_template(:new)
      end
    end
  end
end
