require 'rails_helper'

RSpec.describe ReqresApiService do

  describe "ReqRes Api Service" do
    it "should find a existent user and return true" do
      begin
        response = ReqresApiService.search_user({:name => 'Emma'}) 
        expect(response).should be_true
      rescue
        'An error has occured.'
      end
    end

    it "should not found a user and return false" do
      begin
        response = ReqresApiService.search_user({:name => 'Estudar'}) 
        expect(response).should be_false
      rescue
        'An error has occured.'
      end
    end

    it "should create user with success" do
      begin
        response = ReqresApiService.create_user({:name => 'Estudar', :job => 'Ensino'})
        expect(response).should be_true
      rescue
        'An error has occured.' 
      end
    end

    it "should create user and has id" do
      begin
        response = ReqresApiService.create_user({:name => 'Estudar', :job => 'Ensino'})
        expect(response['id']).should_not be_nil
      rescue
        'An error has occured.'
      end
    end

  end

end