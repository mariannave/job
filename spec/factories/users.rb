FactoryBot.define do
  factory :user do
    name { "MyString" }
    last_name { "MyString" }
    job { "MyString" }
    remote_id { 1 }
  end
end