require "rails_helper"

RSpec.describe CreateUserInApi do

  before do
    WebMock.allow_net_connect!
    VCR.turn_off!
  end

  describe ".call" do
    it "succeeds" do
      context = CreateUserInApi.call(name: "Estudar", last_name: "Org", job: "Dev")
      expect(context).to be_a_success
    end

    it "failure" do
      context = CreateUserInApi.call(name: "Emma", last_name: "Wong", job: "Dev")
      expect(context).to be_a_failure
    end
  end
end