require 'rails_helper'

RSpec.describe FindUserInAPI do

  before do
    WebMock.allow_net_connect!
    VCR.turn_off!
  end

  describe ".call" do
    it "succeeds" do
      context = FindUserInAPI.call(name: "Estudar", last_name: "Org")
      expect(context).to be_a_success
    end

    it "failure" do
      context = FindUserInAPI.call(name: "Emma", last_name: "Wong")
      expect(context).to be_a_failure
    end
  end
end