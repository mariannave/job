require "rails_helper"

RSpec.describe CreateUserInDatabase do

  before do
    WebMock.allow_net_connect!
    VCR.turn_off!
  end

  describe ".call" do
    it "succeeds" do
      context = CreateUserInDatabase.call(name: "Estudar", last_name: "Org", job: "Dev", remote_id: 1)
      expect(context).to be_a_success
    end

    it "failure" do
      context = CreateUserInDatabase.call(name: "", last_name: "", job: "")
      expect(context).to be_a_failure
    end
  end
end
