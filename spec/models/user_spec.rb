require 'rails_helper'

RSpec.describe User, :type => :model do
  subject { described_class.new(name: "Name", last_name: "Last Name", job: "Job") }

  describe "Validations" do
    it "is valid with valid attributes" do
      expect(subject).to be_valid
    end

    it "is not valid without a name" do
      subject.name = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without an last_name" do
      subject.last_name = nil
      expect(subject).to_not be_valid
    end

    it "is not valid without an job" do
      subject.job = nil
      expect(subject).to_not be_valid
    end
  end
end